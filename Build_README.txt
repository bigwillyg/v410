1. Android build
  - Download original android source code ( kitkat 4.4.2 ) from http://source.android.com
  - Untar opensource packages of LGV410_E7_Kitkat_V10d_Android.tar.gz into downloaded android source directory
	a) cat LGV410_E7_Kitkat_V10d_Android.tar.gza* | tar zxvpf -
  - And, merge the source into the android source code
  - Run following scripts to build android
    a) source build/envsetup.sh
    b) lunch
    c) make
  - When you compile the android source code, you have to add google original prebuilt source(toolchain) into the android directory.
  - After build, you can find output at out/target/product/generic

2. Kernel Build  
  - Uncompress using following command at the android directory
        tar xvzf LGV410_E7_Kitkat_V10d_Kernel.tar.gz  
  - When you compile the kernel source code, you have to add google original prebuilt source(toolchain) into the android directory.
  - Run following scripts to build kernel
    a) cd kernel
    c) make ARCH=arm CROSS_COMPILE=../prebuilts/gcc/linux-x86/arm/arm-eabi-4.7/bin/arm-eabi- w7ds_open_cis_defconfig zImage
  - After build, you can find the build image(zImage) at arch/arm/boot

3. how  to build chromium_lge (vendor\lge\external\chromium_lge\src),
   please refer to README.txt at the folder mentioned above.



